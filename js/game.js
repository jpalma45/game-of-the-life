var canvas;
var ctx;
var fps = 30;

var canvasX = 500;
var canvasY = 500;

var tileX, tileY;

//variables relacionadas con el tablero de juego
var tablero;
var filas = 100;
var columnas = 100;

var blanco = '#FFFFFF';
var negro = '#000000';

function creaArray2D(f, c) {
    var obj = new Array(f);
    for(y=0; y<f; y++){
      obj[y]= new Array(c);
    }
  
    return obj;
}

//objeto agente o turinita
var Agente = function(x, y, estado) {
    
    this.x = x;
    this.y = y;
    this.estado = estado;           //vivo = 1, muerto = 0
    this.estadoProx = this.estado;  //estado que tendrá en el siguiente ciclo
  
    this.vecinos = [];    //guardamos el listado de sus vecinos
  
    //Método que añade los vecinos del objeto actual
    this.addVecinos = function(){
      var xVecino;
      var yVecino;
  
      for(i=-1; i<2; i++){
        for(j=-1; j<2; j++){
          xVecino = (this.x + j + columnas) % columnas;
          yVecino = (this.y + i + filas) % filas;
  
  
  
          //descartamos el agente actual (yo no puedo ser mi propio vecino)
          if(i!=0 || j!=0){
            this.vecinos.push(tablero[yVecino][xVecino]);
          }
  
        }
      }
    }

    this.dibuja = function(){

        var color;
    
        if(this.estado == 1){
          color = blanco;
        }
        else{
          color = negro;
        }
    
        ctx.fillStyle = color;
        ctx.fillRect(this.x*tileX, this.y*tileY, tileX, tileY);
    
      }

      //las leyes de conway
      this.nuevoCiclo = function() {
          var suma = 0;

          //calcula la cantidad de vacinos vivos
          for(i = 0; i < this.vecinos.length; i++) {
            suma += this.vecinos[i].estado;
          }

          //Aplicamos las normas
          this.estadoProx = this.estado; // queda igual

          // dead
          if (suma < 2 || suma > 3) {
              this.estadoProx = 0;
          }

          //life / resect
          if (suma == 3) {
              this.estadoProx = 1;
          }

      }

      this.mutacion = function(){
          this.estado = this.estadoProx;
      }

}

function initTablero(obj) {
    var estado;

    for(y=0; y<filas; y++){
      for(x=0; x<columnas; x++){
        estado = Math.floor(Math.random()*2);
        obj[y][x] = new Agente(x, y, estado);
      }
    }
  
  
    for(y=0; y<filas; y++){
      for(x=0; x<columnas; x++){
        obj[y][x].addVecinos();
      }
    }
}

function init() {
    //asociamos el canvas
    canvas = document.getElementById('view');
    ctx = canvas.getContext('2d');

    //Ajustamos el tamaño
    canvas.width = canvasX;
    canvas.height = canvasY;

    //calculo tamaños tiles
    tileX = Math.floor(canvasX / filas);
    tileY = Math.floor(canvasY / columnas);

    //Creamos el tablero
    tablero = creaArray2D(filas, columnas);

    //Ejecutar el bucle principal
    setInterval(function() {principal();}, 1000 / fps);

    //Inicializa tablero
    initTablero(tablero);

}

function dibujaTablero(obj) {

    // dibuja los agentes
    for(y = 0; y < filas; y++){
        for(x = 0; x < columnas; x++){
            obj[y][x].dibuja();
        }
    }

    //calcula es siguiente ciclo
    for(x = 0; x < filas; x++){
        for(y = 0; y < columnas; y++){
            obj[y][x].nuevoCiclo();
        }
    }

    // aplica la mutacion
    for(x = 0; x < filas; x++){
        for(y = 0; y < columnas; y++){
            obj[y][x].mutacion();
        }
    }
}

function borraCanvas() {
    canvas.width = canvas.width;
    canvas.height = canvas.height;
}


function principal() {
    borraCanvas();
    dibujaTablero(tablero);
}